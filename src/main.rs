use std::process;
use std::time::Duration;

use log::{info, LevelFilter};
use log4rs::append::rolling_file::policy::compound::CompoundPolicy;
use log4rs::append::rolling_file::policy::compound::roll::fixed_window::{FixedWindowRollerBuilder};
use log4rs::append::rolling_file::policy::compound::trigger::size::SizeTrigger;
use log4rs::append::rolling_file::RollingFileAppender;
use log4rs::config::{Appender, Config, Root};
use log4rs::encode::pattern::PatternEncoder;
use paho_mqtt::{Client, ConnectOptionsBuilder};
use clap::Parser;

#[derive(Parser)]
struct Opts {
    #[clap(short, long, default_value("tcp://localhost:1883"))]
    broker: String,
    #[clap(short, long, default_value("1883"))]
    port: u16,
    #[clap(short, long = "topic", default_value("test/mqtt"))]
    topics: Vec<String>,

    #[clap(long, default_value("/tmp"))]
    log_dir_path: std::path::PathBuf,
    #[clap(long, default_value("mqtt.log"))]
    log_file_name: String,
    #[clap(long, default_value("{d(%Y-%m-%d %H:%M:%S)}-{m}{n}"))]
    pattern: String,
    #[clap(long, default_value("10"))]
    rotate_file_count: u32,
    #[clap(long, default_value("250"))]
    rotate_size_trigger: u64,
}

fn main() {
    let opts: Opts = Opts::parse();
    // Set up logging
    let log_pattern = Box::new(PatternEncoder::new(&opts.pattern));
    let log_file_path = opts.log_dir_path.join(opts.log_file_name);
    let log_file_path_backup_pattern = log_file_path.to_str().unwrap().to_string() + ".{}";

    let size_trigger = Box::new(SizeTrigger::new(opts.rotate_size_trigger));
    let roller = Box::new(FixedWindowRollerBuilder::default().build(&log_file_path_backup_pattern, opts.rotate_file_count).unwrap());
    let rotate_policy = Box::new(CompoundPolicy::new(size_trigger, roller));
    let file_appender = Box::new(RollingFileAppender::builder()
            .encoder(log_pattern)
            .build(log_file_path, rotate_policy)
            .unwrap());

    let config = Config::builder()
        .appender(Appender::builder().build("file", file_appender))
        .build(Root::builder().appender("file").build(LevelFilter::Info))
        .unwrap();

    log4rs::init_config(config).unwrap();

    // Connect to the broker
    let cli = Client::new(opts.broker).unwrap_or_else(|err| {
        println!("Error creating the client: {:?}", err);
        process::exit(1);
    });

    let conn_opts = ConnectOptionsBuilder::new()
        .keep_alive_interval(Duration::from_secs(20))
        .clean_session(true)
        .finalize();

    // Connect and wait for it to complete or fail
    if let Err(e) = cli.connect(conn_opts) {
        println!("Unable to connect:\n\t{:?}", e);
        process::exit(1);
    }

    if let Err(e) = cli.subscribe_many(&opts.topics, &vec![0;opts.topics.len()]) {
        println!("Unable to subscribe:\n\t{:?}", e);
        process::exit(1);
    }

    // Subscribe to a topic and provide a closure to be executed when a message
    // arrives.
    let rx = cli.start_consuming();

    for msg in rx.iter() {
        if let Some(msg) = msg {
            info!("{} {}", msg.topic(), msg.payload_str());
        }
    }
}