#!/usr/bin/env bash

set -e

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(git rev-parse --show-toplevel)

ID=${1:-"0"}

# id	sequence	hostname	username	password	wifiSSID	wifiPassword	rotateFileCount	rotateSizeTrigger (Mo)	logDirPath	pattern
# 3bfc18b2-bf72-462e-a2a1-0576e55627a4	0000	tauros-0000	ash	catchThemOnceForAll-0000	safariParc-0	WLcZzHHDubR59S8sU98RKJE-0	100	150	/home/ash/data	{d(%Y-%m-%d %H:%M:%S)} {m}{n}
# c6522b3c-cc53-458b-b76d-c57f5d29cf3e	0001	tauros-0001	ash	catchThemOnceForAll-0001	safariParc-1	WLcZzHHDubR59S8sU98RKJE-1	100	150	/home/ash/data	{d(%Y-%m-%d %H:%M:%S)} {m}{n}
# 385892f3-a3f4-490a-94f8-8c22b51ebd94	0002	tauros-0002	ash	catchThemOnceForAll-0002	safariParc-2	WLcZzHHDubR59S8sU98RKJE-2	100	150	/home/ash/data	{d(%Y-%m-%d %H:%M:%S)} {m}{n}
# 4edcc5b6-a161-4369-bec9-a372c4f2b1c3	0003	tauros-0003	ash	catchThemOnceForAll-0003	safariParc-3	WLcZzHHDubR59S8sU98RKJE-3	100	150	/home/ash/data	{d(%Y-%m-%d %H:%M:%S)} {m}{n}
# 7321269d-43c9-44f2-969d-d0621ba68ab4	0004	tauros-0004	ash	catchThemOnceForAll-0004	safariParc-4	WLcZzHHDubR59S8sU98RKJE-4	100	150	/home/ash/data	{d(%Y-%m-%d %H:%M:%S)} {m}{n}

${repoDir}/scripts/raspberry/flash/index.bash \
  --image ~/Downloads/ubuntu-22.10-preinstalled-server-arm64+raspi.img \
  --device /dev/mmcblk0 \
  --hostname tauros-000${ID} \
  --username ash \
  --password catchThemOnceForAll-000${ID} \
  --wifi-ssid safariParc-${ID} \
  --wifi-password WLcZzHHDubR59S8sU98RKJE-${ID} \
  --mqtt-storage-args "--rotate-file-count 100 \
    --rotate-size-trigger 150000000 \
    --log-dir-path /home/ash/data \
    --pattern '{d(%Y-%m-%d %H:%M:%S)} {m}{n}' \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/BME280/humidity\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/BME280/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/BME280/pressure\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/MPU9250/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/MPU9250/acceleration\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/MPU9250/gyroscope\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/MPU9250/compass\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/SHTC3/temparature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/0/SHTC3/humidity\\\"\
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/BME280/humidity\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/BME280/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/BME280/pressure\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/MPU9250/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/MPU9250/acceleration\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/MPU9250/gyroscope\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/MPU9250/compass\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/SHTC3/temparature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/1/SHTC3/humidity\\\"\
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/BME280/humidity\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/BME280/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/BME280/pressure\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/MPU9250/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/MPU9250/acceleration\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/MPU9250/gyroscope\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/MPU9250/compass\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/SHTC3/temparature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/2/SHTC3/humidity\\\"\
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/BME280/humidity\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/BME280/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/BME280/pressure\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/MPU9250/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/MPU9250/acceleration\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/MPU9250/gyroscope\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/MPU9250/compass\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/SHTC3/temparature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/3/SHTC3/humidity\\\"\
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/BME280/humidity\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/BME280/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/BME280/pressure\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/MPU9250/temperature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/MPU9250/acceleration\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/MPU9250/gyroscope\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/MPU9250/compass\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/SHTC3/temparature\\\" \
    --topic \\\"3bfc18b2-bf72-462e-a2a1-0576e55627a4/4/SHTC3/humidity\\\"\
  "