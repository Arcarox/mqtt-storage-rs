#!/usr/bin/env bash

set -v

# This script is used to setup a raspberry pi 4 4G as:
# - a wifi access point
# - a mqtt broker
# - a mqtt storage service

# Variables
WIFI_SSID=${1:-"MyWifi"}
WIFI_PASSWORD=${2:-"MyPassword"}
MQTT_STORAGE_ARGS=${3:-""}

# Disable debconf dialogs
export DEBIAN_FRONTEND=noninteractive

# Update the system
sudo apt-get update -y
sudo apt-get upgrade -y

# Install net tools
sudo apt install net-tools -y
sudo apt install network-manager -y

sudo nmcli con add type wifi ifname wlan0 con-name ${WIFI_SSID} autoconnect yes ssid ${WIFI_SSID}
sudo nmcli con modify ${WIFI_SSID} 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
sudo nmcli con modify ${WIFI_SSID} wifi-sec.key-mgmt wpa-psk
sudo nmcli con modify ${WIFI_SSID} wifi-sec.psk "${WIFI_PASSWORD}"
sudo nmcli con up ${WIFI_SSID}

# Install mosquitto
sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa -y
sudo apt-get update -y
sudo apt-get install mosquitto mosquitto-clients -y

cat <<EOF | sudo tee /etc/mosquitto/conf.d/default.conf
persistence true
autosave_interval 1
autosave_on_changes true
queue_qos0_messages true
max_queued_bytes 1000000
max_queued_messages 2500

listener 1883
allow_anonymous true
EOF

cat <<EOF | sudo tee /lib/systemd/system/mqtt-storage.service
[Unit]
Description=MQTT Storage Service
After=mosquitto.service

[Service]
Type=simple
ExecStart=/root/mqtt-storage ${MQTT_STORAGE_ARGS}
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable mqtt-storage.service
sudo systemctl start mqtt-storage.service

sudo systemctl reboot
